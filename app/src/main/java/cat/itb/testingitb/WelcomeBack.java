package cat.itb.testingitb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WelcomeBack extends AppCompatActivity {

    private Button button;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_back);

        button = findViewById(R.id.buttonWelcome);
        textView = findViewById(R.id.weclomeBackText);

        Bundle b = getIntent().getExtras();

        String s = b.getString("username");
        textView.setText("Welcome back "+s);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WelcomeBack.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}