package cat.itb.testingitb;

import androidx.test.espresso.ViewAssertion;
import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions;
import com.schibsted.spain.barista.interaction.BaristaClickInteractions;
import com.schibsted.spain.barista.rule.BaristaRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.doubleClick;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class  MainActivityTest {

    private String USER_TO_BE_TYPED = "roger";
    private String PASS_TO_BE_TYPED = "roger";

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);
    @Rule
    public BaristaRule<MainActivity> baristaRule = BaristaRule.create(MainActivity.class);

    @Test
    public void elements_on_MainActivity_are_displayed_correctly() {
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));
    }

    @Test
    public void text_display_correctly() {
        onView(withId(R.id.textView)).check(matches(withText(R.string.main_activity_title)));
        onView(withId(R.id.button)).check(matches(withText(R.string.next)));
    }

    @Test
    public void button_clicked() {
        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Back")));
    }

    @Test
    public void insert_text_on_fields() {
        onView(withId(R.id.editText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.editTextPass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Logged")));
    }

    @Test
    public void try_navigation() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.welcomeBackActivity)).check(matches(isDisplayed()));

    }

    @Test
    public void try_navigation_barista() {
        BaristaClickInteractions.clickOn(R.id.button);
        BaristaVisibilityAssertions.assertDisplayed(R.id.welcomeBackActivity);
    }

    @Test
    public void try_navigation_return() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.welcomeBackActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.buttonWelcome)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));

        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.welcomeBackActivity)).perform(pressBack());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
    }

    @Test
    public void large_test_function() {
        onView(withId(R.id.editText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.editTextPass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.welcomeBackActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.weclomeBackText)).check(matches(withText("Welcome back "+USER_TO_BE_TYPED)));
        onView(withId(R.id.buttonWelcome)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.editText)).check(matches(withText("")));
        onView(withId(R.id.editTextPass)).check(matches(withText("")));

    }
}

